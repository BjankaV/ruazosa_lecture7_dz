package hr.fer.tel.ruazosa.lectures.net.retrofit

import android.util.Log
import com.squareup.okhttp.RequestBody

import java.io.IOException

import hr.fer.tel.ruazosa.lectures.entity.Course
import hr.fer.tel.ruazosa.lectures.entity.Person
import hr.fer.tel.ruazosa.lectures.entity.ShortCourse
import hr.fer.tel.ruazosa.lectures.entity.ShortPerson
import hr.fer.tel.ruazosa.lectures.net.RestFactory
import hr.fer.tel.ruazosa.lectures.net.RestInterface
import org.json.JSONObject
import retrofit.RestAdapter

class RestRetrofit : RestInterface {
    private val service: LecturesService

    init {
        val baseURL = "http://" + RestFactory.BASE_IP + ":8080/api/"
        val retrofit = RestAdapter.Builder()
                .setEndpoint(baseURL)
                .build()

        service = retrofit.create(LecturesService::class.java)
    }

    override fun getListOfCourses(): List<ShortCourse>? {
        return service.listOfCourses
    }

    override fun getCourse(id: Long?): Course? {
        return service.getCourse(id)
    }

    override fun updateCourse(id: Long?, requestBody: Course): Boolean? {
        service.updateCourse(id, requestBody)
        return true
    }

    override fun getCourseStudents(courseId: Long?): List<ShortPerson>? {
        return service.getCourseStudents(courseId)
    }

    override fun getListOfPersons(): List<ShortPerson>? {
        return service.listOfPersons
    }

    override fun getPerson(id: Long?): Person? {
        return service.getPerson(id)
    }

    override fun removePerson(id: Long?): Boolean? {
        service.removePerson(id)
        return true
    }

    override fun getEnrolledCourses(id: Long?): List<ShortCourse>? {
        return service.getEnrolledCourses(id)
    }

    override fun getTeachingCourses(id: Long?): List<ShortCourse>? {
        return service.getTeachingCourses(id)
    }

    override fun enrollPersonToCourse(courseId: Long?, personId: Long?): Boolean? {
        service.enrollPersonToCourse(courseId, personId)
        return true
    }

    override fun disenrollPersonFromCourse(courseId: Long?, personId: Long?): Boolean? {
        service.disenrollPersonFromCourse(courseId, personId)
        return true
    }
}
