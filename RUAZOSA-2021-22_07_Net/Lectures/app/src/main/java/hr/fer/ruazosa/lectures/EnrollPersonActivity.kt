package hr.fer.ruazosa.lectures

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import hr.fer.tel.ruazosa.lectures.entity.ShortCourse
import hr.fer.tel.ruazosa.lectures.entity.ShortPerson
import hr.fer.tel.ruazosa.lectures.net.RestFactory


class EnrollPersonActivity : AppCompatActivity() {

    private var listView: ListView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enroll_person)

        val shortCourse = intent.getSerializableExtra("course") as ShortCourse

        listView = findViewById(R.id.listPersonsView) as ListView
        listView?.onItemClickListener = AdapterView.OnItemClickListener { parent, _, position, _ ->
            val itemAtPosition = parent.getItemAtPosition(position)
            val shortPerson = itemAtPosition as ShortPerson
            EnrollPersonTask(shortCourse, shortPerson).execute()
            var data: Intent = Intent()
            data.putExtra("course", shortCourse)
            setResult(RESULT_OK, data)
            finish()
        }

        LoadPersonsTask().execute(shortCourse)
    }

    private inner class LoadPersonsTask: AsyncTask<ShortCourse, Void, List<ShortPerson>?>() {
        override fun doInBackground(vararg sCourse: ShortCourse): List<ShortPerson>? {
            val rest = RestFactory.instance
            val course = rest.getCourse(sCourse[0].id)
            val teacher = course?.teacher
            val persons = rest.getListOfPersons()
            val students = rest.getCourseStudents(sCourse[0].id)
            var list: List<ShortPerson>? = null

            if (persons != null && students != null) {
                list = persons - students
            }

            if (list != null && teacher != null) {
                list = list - teacher
            }

            return list
        }

        override fun onPostExecute(persons: List<ShortPerson>?) {
            updatePersonList(persons)
        }
    }

    private fun updatePersonList(persons: List<ShortPerson>?) {
        if(persons != null) {
            val adapter = PersonAdapter(this,
                android.R.layout.simple_list_item_1, persons)
            listView?.adapter = adapter
        } else {
            // TODO show that courses can not be loaded
        }
    }

    private inner class PersonAdapter(context: Context, textViewResourceId: Int, private val shortPersonList: List<ShortPerson>) : ArrayAdapter<ShortPerson>(context, textViewResourceId, shortPersonList)

    private inner class EnrollPersonTask(val sCourse: ShortCourse, val sPerson: ShortPerson): AsyncTask<Void, Void, Boolean?>() {

        override fun doInBackground(vararg params: Void): Boolean? {
            val rest = RestFactory.instance

            return rest.enrollPersonToCourse(sCourse.id, sPerson.id)
        }
    }
}