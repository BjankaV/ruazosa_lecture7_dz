package hr.fer.tel.ruazosa.lectures.net

import com.squareup.okhttp.RequestBody
import hr.fer.tel.ruazosa.lectures.entity.Course
import hr.fer.tel.ruazosa.lectures.entity.Person
import hr.fer.tel.ruazosa.lectures.entity.ShortCourse
import hr.fer.tel.ruazosa.lectures.entity.ShortPerson
import org.json.JSONObject
import retrofit.http.Path

interface RestInterface {
    fun getListOfCourses(): List<ShortCourse>?
    fun getListOfPersons(): List<ShortPerson>?

    fun getCourse(id: Long?): Course?
    fun updateCourse(id: Long?, requestBody: Course): Boolean?
    fun getCourseStudents(courseId: Long?): List<ShortPerson>?
    fun getPerson(id: Long?): Person?
    fun removePerson(id: Long?): Boolean?

    fun getEnrolledCourses(id: Long?): List<ShortCourse>?
    fun getTeachingCourses(id: Long?): List<ShortCourse>?

    fun enrollPersonToCourse(courseId: Long?, personId: Long?): Boolean?
    fun disenrollPersonFromCourse(courseId: Long?, personId: Long?): Boolean?
}
