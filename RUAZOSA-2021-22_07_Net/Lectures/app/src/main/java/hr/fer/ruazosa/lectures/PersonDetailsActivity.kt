package hr.fer.ruazosa.lectures

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.AsyncTask
import android.util.Log
import android.widget.Button
import android.widget.TextView
import hr.fer.tel.ruazosa.lectures.entity.Person
import hr.fer.tel.ruazosa.lectures.entity.ShortCourse
import hr.fer.tel.ruazosa.lectures.entity.ShortPerson
import hr.fer.tel.ruazosa.lectures.net.RestFactory
import kotlinx.android.synthetic.main.activity_person_details.*
import org.json.JSONObject

class PersonDetailsActivity : AppCompatActivity() {

    private var person: Person? = null
    private var firstName: TextView? = null
    private var lastName: TextView? = null
    private var room: TextView? = null
    private var phone: TextView? = null
    private var disenrollPersonButton: Button? = null
    private var deletePersonButton: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_person_details)

        firstName = findViewById(R.id.personFirstName) as TextView
        lastName = findViewById(R.id.personLastName) as TextView
        room = findViewById(R.id.personRoom) as TextView
        phone = findViewById(R.id.personPhone) as TextView
        disenrollPersonButton = findViewById(R.id.disenrollPersonButton) as Button
        deletePersonButton = findViewById(R.id.deletePersonButton) as Button

        val shortPerson = intent.getSerializableExtra("person") as ShortPerson
        val shortCourse = intent.getSerializableExtra("course") as ShortCourse

        LoadShortPersonTask().execute(shortPerson)

        disenrollPersonButton?.setOnClickListener {
            DisenrollPersonTask(shortCourse, shortPerson).execute()
            var data: Intent = Intent()
            data.putExtra("course", shortCourse)
            setResult(RESULT_OK, data)
            finish()
        }

        deletePersonButton?.setOnClickListener {
            DeletePersonTask(shortPerson).execute()
            var data: Intent = Intent()
            data.putExtra("course", shortCourse)
            setResult(RESULT_OK, data)
            finish()
        }
    }

    private inner class LoadShortPersonTask: AsyncTask<ShortPerson, Void, Person?>() {

        override fun doInBackground(vararg sPerson: ShortPerson): Person? {
            val rest = RestFactory.instance
            return rest.getPerson(sPerson[0].id)
        }

        override fun onPostExecute(newPerson: Person?) {
            person = newPerson
            firstName?.text = person?.firstName
            lastName?.text = person?.lastName
            room?.text = person?.room
            phone?.text = person?.phone
        }
    }

    private inner class DisenrollPersonTask(val sCourse: ShortCourse, val sPerson: ShortPerson): AsyncTask<Void, Void, Boolean?>() {

        override fun doInBackground(vararg params: Void): Boolean? {
            val rest = RestFactory.instance

            return rest.disenrollPersonFromCourse(sCourse.id, sPerson.id)
        }
    }

    private inner class DeletePersonTask(val sPerson: ShortPerson): AsyncTask<Void, Void, Boolean?>() {

        override fun doInBackground(vararg params: Void): Boolean? {
            val rest = RestFactory.instance

            val courses = rest.getListOfCourses()

            if (courses != null) {
                for(sCourse in courses) {
                    val course = rest.getCourse(sCourse.id)
                    val students = rest.getCourseStudents(sCourse.id)
                    val teacher = course?.teacher

                    if(students?.contains(sPerson) == true) {
                        rest.disenrollPersonFromCourse(sCourse.id, sPerson.id)
                    }
                    if(teacher?.equals(sPerson) == true) {

                        course?.teacher = null
                        rest.updateCourse(sCourse.id, course)

                        if (students != null) {
                            for(student in students) {
                                rest.enrollPersonToCourse(sCourse.id, student.id)
                            }
                        }
                    }
                }
            }

            return rest.removePerson(sPerson.id)
        }
    }
}