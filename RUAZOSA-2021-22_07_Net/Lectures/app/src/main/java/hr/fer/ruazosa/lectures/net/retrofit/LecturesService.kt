package hr.fer.tel.ruazosa.lectures.net.retrofit


import com.squareup.okhttp.RequestBody
import com.squareup.okhttp.Response
import com.squareup.okhttp.ResponseBody
import hr.fer.tel.ruazosa.lectures.entity.Course
import hr.fer.tel.ruazosa.lectures.entity.Person
import hr.fer.tel.ruazosa.lectures.entity.ShortCourse
import hr.fer.tel.ruazosa.lectures.entity.ShortPerson
import org.json.JSONObject
import org.springframework.http.ResponseEntity
import retrofit.ResponseCallback
import retrofit.http.*

interface LecturesService {
    @get:GET("/courses")
    val listOfCourses: List<ShortCourse>

    @GET("/courses/{id}")
    fun getCourse(@Path("id") id: Long?): Course

    @PUT("/courses/{id}")
    fun updateCourse(@Path("id") id: Long?, @Body requestBody: Course): ResponseBody

    @GET("/courses/{id}/students")
    fun getCourseStudents(@Path("id") courseId: Long?): List<ShortPerson>

    @get:GET("/persons")
    val listOfPersons: List<ShortPerson>

    @GET("/persons/{id}")
    fun getPerson(@Path("id") id: Long?): Person

    @DELETE("/persons/{id}")
    fun removePerson(@Path("id") id: Long?): ResponseBody

    @GET("/persons/{id}/enrolledCourses")
    fun getEnrolledCourses(@Path("id") id: Long?): List<ShortCourse>

    @GET("/persons/{id}/teachingCourses")
    fun getTeachingCourses(@Path("id") id: Long?): List<ShortCourse>

    @POST("/courses/{courseId}/enrollPerson/{personId}")
    fun enrollPersonToCourse(@Path("courseId") courseId: Long?, @Path("personId") personId: Long?, @Body requestBody: Any = Object()): ResponseBody

    @POST("/courses/{courseId}/unenrollPerson/{personId}")
    fun disenrollPersonFromCourse(@Path("courseId") courseId: Long?, @Path("personId") personId: Long?, @Body requestBody: Any = Object()): ResponseBody
}
