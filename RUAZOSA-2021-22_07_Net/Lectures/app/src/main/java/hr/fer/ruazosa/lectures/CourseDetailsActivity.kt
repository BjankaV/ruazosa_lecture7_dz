package hr.fer.ruazosa.lectures

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.*
import hr.fer.tel.ruazosa.lectures.entity.Course
import hr.fer.tel.ruazosa.lectures.entity.ShortCourse
import hr.fer.tel.ruazosa.lectures.entity.ShortPerson
import hr.fer.tel.ruazosa.lectures.net.RestFactory

class CourseDetailsActivity : AppCompatActivity() {

    private var course: Course? = null
    private var courseName: TextView? = null
    private var courseDescription: TextView? = null
    private var teacher: TextView? = null
    private var courseStudentsButton: Button? = null
    private var enrollPersonButton: Button? = null
    private var listView: ListView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_course_details)

        courseName = findViewById(R.id.courseName) as TextView
        courseDescription = findViewById(R.id.courseDescription) as TextView
        teacher = findViewById(R.id.courseTeacher) as TextView
        courseStudentsButton = findViewById(R.id.courseStudentsButton) as Button
        enrollPersonButton = findViewById(R.id.enrollPersonButton) as Button

        listView = findViewById(R.id.listStudentsView) as ListView


        val shortCourse = intent.getSerializableExtra("course") as ShortCourse

        LoadShortCourseTask().execute(shortCourse)

        courseStudentsButton?.setOnClickListener {
            LoadStudentsTask().execute(shortCourse)
        }

        listView?.onItemClickListener = AdapterView.OnItemClickListener { parent, _, position, _ ->
            val itemAtPosition = parent.getItemAtPosition(position)
            val shortPerson = itemAtPosition as ShortPerson
            val intent = Intent(this@CourseDetailsActivity, PersonDetailsActivity::class.java)
            intent.putExtra("person", shortPerson)
            intent.putExtra("course", shortCourse)
            startActivityForResult(intent, 2)
        }

        enrollPersonButton?.setOnClickListener {
            val intent = Intent(this@CourseDetailsActivity, EnrollPersonActivity::class.java)
            intent.putExtra("course", shortCourse)
            startActivityForResult(intent, 1)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == 1 || requestCode == 2) {
            if(resultCode == RESULT_OK) {
                val shortCourse = data?.getSerializableExtra("course") as ShortCourse
                LoadStudentsTask().execute(shortCourse)
            }
        }
    }

    private inner class LoadShortCourseTask: AsyncTask<ShortCourse, Void, Course?>() {

        override fun doInBackground(vararg sCourse: ShortCourse): Course? {
            val rest = RestFactory.instance
            return rest.getCourse(sCourse[0].id)
        }

        override fun onPostExecute(newCourse: Course?) {
            course = newCourse
            courseName?.text = course?.name
            courseDescription?.text = course?.description

            this@CourseDetailsActivity.teacher?.text = course?.teacher?.name
        }
    }

    private inner class LoadStudentsTask: AsyncTask<ShortCourse, Void, List<ShortPerson>?>() {
        override fun doInBackground(vararg sCourse: ShortCourse): List<ShortPerson>? {
            val rest = RestFactory.instance

            return rest.getCourseStudents(sCourse[0].id)
        }

        override fun onPostExecute(students: List<ShortPerson>?) {
            updateStudentsList(students)
        }
    }

    private fun updateStudentsList(students: List<ShortPerson>?) {
        if(students != null) {
            val adapter = StudentsAdapter(this,
                android.R.layout.simple_list_item_1, students)
            listView?.adapter = adapter
        } else {
            // TODO show that students can not be loaded
        }
    }

    private inner class StudentsAdapter(context: Context, textViewResourceId: Int, private val shortStudentList: List<ShortPerson>) : ArrayAdapter<ShortPerson>(context, textViewResourceId, shortStudentList)
}
